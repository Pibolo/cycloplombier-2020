<div class="btn_resa d-block d-md-none">
  <a class="btn btn_red d-block mx-auto" href="<?php the_field('reservation_link'); ?>" title="Réservez une intervention">Réserver</a>
</div>
