<?php
/*
Template Name: Réservation
*/

get_header(); ?>
<div class="site-content presentation resa">
  <section class="container-fluid entete-pages text-center">
    <div class="overlay"></div>
    <?php the_post_thumbnail('post-thumbnail', ['class' => 'img-fluid', 'title' => 'Feature image']); ?>
    <h1 class="text-center"><?php the_field('titre_h1'); ?></h1>
  </section>
  <section class="container">
    <?php include(TEMPLATEPATH . "/breadcrumb.php"); ?>
    <?php the_content(); ?>
  </section>

  <?php get_footer(); ?>

  <script>
    jQuery(document).ready(function($) {
      $(window).on('load', function() {
        $('html, body').animate({
          scrollTop: $('.site-content').offset().top + 300
        }, 1000);
      });

      $(document).on('click', '.wpforms-image-choices-item', function() {
        if ($(window).width() < 768) {
          $('.site-content, html, body').stop().animate({
            scrollTop: $("footer").offset().top - 400
          }, 700);
        }
      });

      $(document).on('click', '.wpforms-page-button', function() {
        $('.site-content, html, body').animate({
          scrollTop: 200
        }, 'slow');
      });

    });
  </script>
</div>

</div>
</div>
</div>