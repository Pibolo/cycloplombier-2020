      <?php get_header(); ?>
      <div class="site-content article">
        <section class="container-fluid entete-pages">
          <div class="overlay"></div>
          <?php the_post_thumbnail('post-thumbnail', ['class' => 'img-fluid', 'title' => 'Feature image']); ?>
          <h1 class="text-center"><?php the_title(); ?></h1>
        </section>
        <div class="container">
          <?php include(TEMPLATEPATH . "/breadcrumb.php"); ?>
          <div class="row">
            <?php if (have_posts()) : ?>
              <?php while (have_posts()) : the_post(); ?>
                <div class="col-12 col-md-8">
                  <article>
                    <div class="row">
                      <div class="col-12">
                        <?php
                        $count_posts = wp_count_posts();
                        ?>
                        Par <strong><?php the_author(); ?></strong>&nbsp;-&nbsp;
                        <span class="date">
                          <?php the_date(); ?> -
                        </span>
                        <span class="flag">
                          &nbsp;<?php the_category(', '); ?>
                        </span>
                      </div>
                    </div>
                    <br>
                    <div class="row">
                      <div class="col-12">
                        <?php the_content(['class' => 'img-fluid', 'title' => 'Feature image']); ?>
                      </div>
                    </div>
                  </article>
                </div>
                <div class="col-12 col-md-4">
                  <div class="sidebar_article">
                    <div class="article_new_article">
                      <h4>Derniers articles</h4>
                      <?php
                      $catquery = new WP_Query('offset=1&cat=&posts_per_page=3');
                      ?>
                      <?php while ($catquery->have_posts()) : $catquery->the_post(); ?>
                        <div class="row">
                          <div class="col-12">
                            <a href="<?php the_permalink() ?>">
                              <?php the_post_thumbnail('post-thumbnail', ['class' => 'img-fluid', 'title' => 'Feature image']); ?></a>
                          </div>
                          <div class="col-12">
                            <h5><?php the_title(); ?></h5>
                            <span class="date">
                              <?php the_date(); ?> -
                            </span>
                            <span class="flag">
                              &nbsp;<?php the_category(', '); ?>
                            </span>
                          </div>
                        </div>
                      <?php endwhile; ?>
                      <?php wp_reset_postdata(); ?>
                    </div>
                  </div>
                </div>
          </div>
        <?php endwhile; ?>
      <?php endif; ?>
        </div>
        <?php get_footer(); ?>
        <?php include(TEMPLATEPATH . "/resa.php"); ?>
      </div>
      </div>
      </div>
      </div>