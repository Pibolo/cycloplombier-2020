<?php
/*
Template Name: 404
*/
get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<section class="error-404 not-found">
				<img src="<?php bloginfo('template_directory'); ?>/assets/img/bg_404.png" alt="Ensemble représentant emmêlement de tuyaux" class="img-fluid">
				<div class="page-content">
						<h1 style="color: #000;">Erreur 404</h1>
	 					<p>Oups, ceci est une erreur dite 404 ! En
						termes simples, la page ne peut être trouvée...</p>
						<a href="<?php echo esc_url(home_url('/')); ?>" class="btn btn_red">Revenez donc voir votre plombier :)</a>
				</div><!-- .page-content -->
			</section><!-- .error-404 -->

		</main><!-- .site-main -->
	</div><!-- .content-area -->

<?php get_footer(); ?>
<?php include(TEMPLATEPATH . "/resa.php"); ?>
