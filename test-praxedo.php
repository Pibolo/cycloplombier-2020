<?php
/*
Template Name: Test praxedo
*/
$fields = [
    'clientname' => 'Rouge',
    'clientmail' => 'Vert',
    'clientPhone' => 'Bleu',
];


$addIntervention = vsprintf('
<soap:Envelope xmlns:soap="http://www.w3.org/2003/05/soap-envelope">
	<soap:Header/>
	<soap:Body>
		<ns2:createCustomers xmlns:ns2="http://ws.praxedo.com/v6/customermodel">
			<customers>
				<address>64, rue Saint Lazare</address>
				<city>Paris</city>
				<contact>M. Dupont</contact>
				<contacts>
					<coordinates>0141020305</coordinates>
					<flags>0</flags>
					<type>PHONE</type>
				</contacts>
				<contacts>
					<coordinates>0141020304</coordinates>
					<flags>0</flags>
					<type>FAX</type>
				</contacts>
				<customAttributes>
					<key>attribute</key>
					<value>custom value</value>
				</customAttributes>
				<description>Description du client</description>
				<id>CUSTOMER001</id>
				<name>Praxedo</name>
				<parentCustomer>PARENTCUSTOMER</parentCustomer>
				<zipCode>75009</zipCode>
			</customers>
			<customers>
				<address>Bercy</address>
				<city>Paris</city>
				<id>CUSTOMER002</id>
				<name>Ministère</name>
				<parentCustomer>PARENTCUSTOMER</parentCustomer>
			</customers>
			<customers>
				<city>Marseille</city>
				<contact>M. Durand</contact>
				<contacts>
					<coordinates>0494202122</coordinates>
					<flags>0</flags>
					<type>PHONE</type>
				</contacts>
				<description>Bureau d\'étude en énergies</description>
				<id>CUSTOMER003</id>
				<name>Energie Conseil</name>
				<organizationalUnit>INTERNE/</organizationalUnit>
				<parentCustomer>PARENTCUSTOMER</parentCustomer>
			</customers>
		</ns2:createCustomers>
	</soap:Body>
</soap:Envelope>
        ', $fields);

echo $addIntervention;