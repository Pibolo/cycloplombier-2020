<!DOCTYPE html>
<html lang="fr">
<head>
  <meta charset="UTF-8">
  <title>Cycloplombier - Premier artisan plombier à vélo sur Paris</title>

  <meta name="robots" content="noindex, nofollow" />
  <meta name="description" content="#####" />


  <!-- Google Authorship and Publisher Markup -->
  <link rel="author" href="https://plus.google.com/[Google+_Profile]/posts"/>
  <link rel="publisher" href="https://plus.google.com/[Google+_Page_Profile]"/>
  <!-- Schema.org markup for Google+ -->
  <meta itemprop="name" content="The Name or Title Here">
  <meta itemprop="description" content="This is the page description">
  <meta itemprop="image" content="http://www.example.com/image.jpg">
  <!-- Twitter Card data -->
  <meta name="twitter:card" content="summary_large_image">
  <meta name="twitter:site" content="@publisher_handle">
  <meta name="twitter:title" content="Page Title">
  <meta name="twitter:description" content="Page description less than 200 characters">
  <meta name="twitter:creator" content="@author_handle">
  <!-- Twitter summary card with large image must be at least 280x150px -->
  <meta name="twitter:image:src" content="http://www.example.com/image.html">
  <!-- Open Graph data -->
  <meta property="og:title" content="Title Here" />
  <meta property="og:type" content="article" />
  <meta property="og:url" content="http://www.example.com/" />
  <meta property="og:image" content="http://example.com/image.jpg" />
  <meta property="og:description" content="Description Here" />
  <meta property="og:site_name" content="Site Name" />
  <meta property="article:published_time" content="2013-09-17T05:59:00+01:00" />
  <meta property="article:modified_time" content="2013-09-16T19:08:47+01:00" />
  <meta property="article:section" content="Article Section" />
  <meta property="article:tag" content="Article Tag" />
  <meta property="fb:admins" content="Facebook numberic ID" />
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />

  <link rel="shortcut icon" href="favicon.ico">
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <![endif]-->

  <!-- Mobile Devices -->
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
  <meta name="format-detection" content="telephone=no" />
  <meta name="mobile-web-app-capable" content="yes" />
  <meta name="mobile-web-app-title" content="Nom du site" />
  <meta name="apple-mobile-web-app-capable" content="yes" />
  <meta name="apple-mobile-web-app-title" content="Nom du site">

  <!--LINK CSS-->

  <link rel="stylesheet" type="text/css" href="./assets/css/styles.css">
  <link rel="stylesheet" href="./assets/css/bootstrap/bootstrap.min.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
  <link rel="stylesheet" href="./assets/css/owl.carousel.min.css">
  <link rel="stylesheet" href="./assets/css/owl.theme.default.min.css">

  <!--LINK JS-->
  <script type="text/javascript" src="./assets/js/jquery/jquery-3.3.1.min.js"></script>
  <script type="text/javascript" src="./assets/js/bootstrap/bootstrap.min.js"></script>
  <script src="./assets/js/owl.carousel.js"></script>


</head>

      <?php include('includes/header.php'); ?>
        <div class="site-content article">
          <section class="container-fluid entete-pages">
            <img src="<?php the_field('img_blog'); ?>" class="img-fluid test" alt="">
            <h1 class="text-center"><?php the_field('titre_h1'); ?></h1>
          </section>

          <div class="container">
            <div class="row">
              <div class="col-12">
                <h2>Actualité</h2>
              </div>
              <div class="col-12 col-md-8">
                <article>
                  <div class="row">
                    <div class="col-12">
                      <h3>On aurait aussi bien pu prendre le bus... Pourquoi le vélo ?</h3>
                      <span class="date">
                      17/09/2018 -
                      </span>
                      <span class="flag">
                      &nbsp;Actualités
                      </span>
                    </div>
                  </div>
                  <br>

                  <div class="row">
                    <div class="col-12">
                      <img src="./assets/img/thumb_actu1.png" class="img-fluid float-left" alt="">
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Incidunt quaerat facere qui magni sequi assumenda quasi iure maiores tempore, nihil cum libero cumque est, iste eos ex autem explicabo officia. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Blanditiis atque, tenetur nisi fuga ipsum laboriosam beatae velit voluptates odit voluptatum explicabo in voluptate nulla eaque consequuntur praesentium debitis, reiciendis? Temporibus.</p>
                      <h4>Un plombier à vélo, concrètement, comment c'est possible ?</h4>
                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Incidunt quaerat facere qui magni sequi assumenda quasi iure maiores tempore, nihil cum libero cumque est, iste eos ex autem explicabo officia.</p>

                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Incidunt quaerat facere qui magni sequi assumenda quasi iure maiores tempore, nihil cum libero cumque est, iste eos ex autem explicabo officia. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nemo qui molestiae placeat dolorem expedita corrupti libero tenetur iste accusantium neque quibusdam repudiandae error eligendi perspiciatis, non inventore cupiditate optio modi!</p>

                      <h4>Tu aurais aussi bien pu prendre le bus... Pourquoi le vélo ?</h4>

                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Mollitia, repellendus, dolorum! Explicabo minus, nam iure, nisi perferendis possimus amet eum earum quos reiciendis pariatur enim dolore. Explicabo nihil, deserunt eveniet?</p>

                      <cite>
                        Et le vélo c'est aussi pour le capital sympathie
                      </cite>

                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iure tenetur sed nisi, enim sequi animi sunt ex perspiciatis ipsam voluptatibus distinctio mollitia. Cum quos voluptatem similique, laboriosam fuga molestias suscipit. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime officia totam dolor, neque voluptas labore rem obcaecati sit, consectetur doloribus corporis dolores laborum nemo perspiciatis esse cupiditate fuga. Qui, cumque.</p><br>

                      <img src="./assets/img/img_bandeau_article.png" class="img-fluid w-100 d-block" alt=""><br>

                      <h4>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Numquam, illo.</h4>

                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo facere, iste ab dolor consequuntur ratione dolores cum velit tempore eos blanditiis recusandae, sapiente non reprehenderit earum animi harum architecto necessitatibus! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum beatae dolore, dolorum architecto rerum ipsa debitis, aperiam enim esse vel sint itaque voluptatibus eum quidem iure deleniti aliquam, atque id.</p>
                      <h4>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Numquam, illo.</h4>

                      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Explicabo facere, iste ab dolor consequuntur ratione dolores cum velit tempore eos blanditiis recusandae, sapiente non reprehenderit earum animi harum architecto necessitatibus! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum beatae dolore, dolorum architecto rerum ipsa debitis, aperiam enim esse vel sint itaque voluptatibus eum quidem iure deleniti aliquam, atque id.</p>
                                      </div>
                    </div>
                </article>
              </div>
              <div class="col-12 col-md-4">
                <div class="sidebar_article">
                  <div class="article_new_article">
                    <h4>Derniers articles</h4>
                    <div class="row">
                      <div class="col-6">
                        <img src="./assets/img/thumb_actu3.png" alt="Nouvel article 1" class="img-fluid">
                      </div>
                      <div class="col-6">
                        <h5>L'avenir du Cyclo ?</h5>
                        <span class="date">
                        17/09/2018 -
                        </span>
                        <span class="flag">
                        &nbsp;Actualités
                        </span>
                      </div>
                      <div class="col-6">
                        <img src="./assets/img/thumb_actu3.png" alt="Nouvel article 1" class="img-fluid">
                      </div>
                      <div class="col-6">
                        <h5>Cycloplombier, le plombier le plus écolo de la capitale</h5>
                        <span class="date">
                        17/09/2018 -
                        </span>
                        <span class="flag">
                        &nbsp;Actualités
                        </span>
                      </div>

                    </div>
                  </div>
                  <div class="article_tags">
                    <h4>Tags</h4>
                    <div class="col-12 tags">
                      <span class="badge badge-secondary all_badge">
                        Tout
                      </span>
                      <span class="badge badge-secondary">
                        Plomberie
                      </span>
                      <span class="badge badge-secondary">
                        Plombier à Paris
                      </span>
                      <span class="badge badge-secondary">
                        Ecologie
                      </span>
                      <span class="badge badge-secondary">
                        Artisanat
                      </span>
                    </div>
                  </div>
                  <div class="article_press">
                    <h4>Presse</h4>
                    <div class="col-12">
                      <a href="#">
                        <div class="row one_press">
                          <div class="img_press">
                            <img src="./assets/img/thumb_actu1.png" alt="Actualité 1" >
                          </div>

                          <div class="block_text_press">
                            <h5>On aurait aussi bien pu prendre le bus... Pourquoi le vélo ?</h5>
                            <span class="date">
                              17/09/2018 -
                            </span>
                            <span class="flag">
                              &nbsp;Actualités
                            </span><br><br>
                          </div>
                        </div>
                      </a>

                      <a href="#">
                        <div class="row one_press">
                          <div class="img_press">
                            <img src="./assets/img/thumb_actu3.png" alt="Actualité 1" >
                          </div>

                          <div class="block_text_press">
                            <h5>On aurait aussi bien pu prendre le bus... Pourquoi le vélo ?</h5>
                            <span class="date">
                              17/09/2018 -
                            </span>
                            <span class="flag">
                              &nbsp;Actualités
                            </span><br><br>
                          </div>
                        </div>
                      </a>

                      <a href="#">
                        <div class="row one_press">
                          <div class="img_press">
                            <img src="./assets/img/thumb_actu2.png" alt="Actualité 1" >
                          </div>

                          <div class="block_text_press">
                            <h5>On aurait aussi bien pu prendre le bus... Pourquoi le vélo ?</h5>
                            <span class="date">
                              17/09/2018 -
                            </span>
                            <span class="flag">
                              &nbsp;Actualités
                            </span><br><br>
                          </div>
                        </div>
                      </a>
                    </div>
                  </div>
                </div>
              </div>


            </div>
          </div>

          <?php include('includes/footer.php'); ?>
        </div>
        <div class="site-cache" id="site-cache">
        </div>
    </div>
</div>



  <script type="text/javascript" src="./assets/js/bootstrap/bootstrap.min.js"></script>
  <script type="text/javascript" src="./assets/js/main.js"></script>

</body>
</html>
