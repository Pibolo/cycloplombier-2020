<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">

<head>
  <meta charset="<?php bloginfo('charset'); ?>">
  <meta name="viewport" content="width=device-width">
  <link rel="profile" href="http://gmpg.org/xfn/11">
  <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

  <!--[if lt IE 9]>
  <script src="<?php echo esc_url(get_template_directory_uri()); ?>/js/html5.js"></script>
  <![endif]-->
  <?php wp_head(); ?>
</head>
<script type="application/ld+json">
  {
    "@context": "http://schema.org",
    "@type": "Organization",
    "name": "Cycloplombier",
    "url": "https://www.cycloplombier.com/",
    "sameAs": [
      "https://www.facebook.com/Cycloplombier/",
      "https://www.instagram.com/cyclo_plombier/",
      "https://www.linkedin.com/company/cyclo-plombier-sas/",
      "https://twitter.com/cycloplombier/"
    ]
  }
</script>

<body <?php body_class(); ?>>
  <header class="site__header container-fluid header">
    <div class="container p-0">
      <nav class="navbar navbar-expand-lg navbar-dark w-100">
        <a class="navbar-brand" href="<?php echo get_home_url(); ?>">
          <img src="<?php bloginfo('template_directory'); ?>/assets/img/logo_white.png" style="background-color: #000; width: 120px;" class="img-fluid mb-4" alt="">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse w-100 flex-md-column menu " id="navbarCollapse">

          <?php
          $loc = get_nav_menu_locations();
          wp_reset_query();
          if (!empty($loc['header-menu-sup'])) {
            $array_menu = wp_get_nav_menu_items($loc['header-menu-sup']);
            _wp_menu_item_classes_by_context($array_menu);
            $menu = array();
            foreach ($array_menu as $m) {
              if (empty($m->menu_item_parent)) {
                $menu[$m->ID] = array();
                $menu[$m->ID]['ID'] = $m->ID;
                $menu[$m->ID]['obj_id'] = intval($m->object_id);
                $menu[$m->ID]['classes'] = $m->classes;
                $menu[$m->ID]['title'] = $m->title;
                $menu[$m->ID]['url'] = $m->url;
                $menu[$m->ID]['children'] = array();
              }
            }
            echo '<ul class="navbar-nav ml-auto d-md-flex justify-content-between sup-menu mb-2">';
            foreach ($menu as $m) {
              $classes = [];
              if (!empty(array_intersect([
                'current-menu-item',
                'current-menu-ancestor',
                'current-menu-parent',
                'current_page_parent',
              ], $m['classes']) && !in_array('btn', $m['classes']))) {
                $classes[] = 'active';
              }
              $mm = false;
              if (get_post_meta($m['ID'], 'mega1', true)) {
                $classes[] = 'dropdown';
                $classes[] = 'position-static';
                $mm = true;
              }
              printf('<li class="nav-item %s">', implode(' ', $classes)); // active ? // dropdown position-static ?
              vprintf('<a class="nav-link" %2$s><span>%1$s</span></a>', array(
                strtoupper($m['title']),
                !empty($m['url']) ? "href=\"{$m['url']}\"" : '',
              ));
              echo '</li>';
            }
          ?>
          <?php
            echo '</ul>';
          }
          ?>
          <?php
          $loc = get_nav_menu_locations();
          wp_reset_query();
          if (!empty($loc['header-menu'])) {
            $array_menu = wp_get_nav_menu_items($loc['header-menu']);
            _wp_menu_item_classes_by_context($array_menu);
            $menu = array();
            foreach ($array_menu as $m) {
              if (empty($m->menu_item_parent)) {
                $menu[$m->ID] = array();
                $menu[$m->ID]['ID'] = $m->ID;
                $menu[$m->ID]['obj_id'] = intval($m->object_id);
                $menu[$m->ID]['classes'] = $m->classes;
                $menu[$m->ID]['title'] = $m->title;
                $menu[$m->ID]['url'] = $m->url;
                $menu[$m->ID]['children'] = array();
              }
            }
            echo '<ul class="navbar-nav ml-auto d-md-flex justify-content-between w-md-100 sub-menu">';
            foreach ($menu as $m) {
              $classes = [];
              if (!empty(array_intersect([
                'current-menu-item',
                'current-menu-ancestor',
                'current-menu-parent',
                'current_page_parent',
              ], $m['classes']) && !in_array('btn', $m['classes']))) {
                $classes[] = 'active';
              }
              $mm = false;
              if (get_post_meta($m['ID'], 'mega1', true)) {
                $classes[] = 'dropdown';
                $classes[] = 'position-static';
                $mm = true;
              }
              printf('<li class="nav-item ml-4 %s">', implode(' ', $classes)); // active ? // dropdown position-static ?
              vprintf('<a class="nav-link" %2$s><span>%1$s</span></a>', array(
                strtoupper($m['title']),
                !empty($m['url']) ? "href=\"{$m['url']}\"" : '',
              ));
              echo '</li>';
            }
          ?>
          <?php
            echo '</ul>';
          }
          ?>
        </div>
      </nav>
    </div>
  </header>