<?php
/*
Template Name: Franchise
*/
get_header(); ?>
<div class="site-content join-us franchise">
    <section class="container-fluid entete-pages text-center">
        <div class="overlay"></div>
        <?php the_post_thumbnail('post-thumbnail', ['class' => 'img-fluid', 'title' => 'Feature image']); ?>
        <h1 class="text-center"><?php the_field('titre_h1'); ?></h1>
    </section>
    <div class="container mb-4 mt-4">
        <?php include(TEMPLATEPATH . "/breadcrumb.php"); ?>
        <h2><?php the_field('titre_h2'); ?></h2>
        <?php the_field('chapeau'); ?><br><br>
        <div class="row">
            <div class="col-12 offset-0 col-md-10 offset-md-1">
                <?php the_content(['class' => 'img-fluid', 'title' => 'Feature image']); ?>
            </div>
        </div>
    </div>
    <?php get_footer(); ?>
    <?php include(TEMPLATEPATH . "/resa.php"); ?>
</div>
</div>
</div>
</div>