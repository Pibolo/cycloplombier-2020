<?php
/*
Template Name: Page Suppression de rdv
*/
get_header(); ?>
<div class="site-content delete-reservation">
    <section class="container-fluid entete-pages">
        <div class="overlay"></div>
        <?php the_post_thumbnail('post-thumbnail', ['class' => 'img-fluid', 'title' => 'Feature image']); ?>
        <h1 class="text-center"><?php the_title(); ?></h1>
    </section>
    <section class="container my-5">
        <?php include(TEMPLATEPATH . "/breadcrumb.php");
        ?>

        <div class="row my-md-5">
            <div class="col-12 py-4">
                <p>Vous souhaitez annuler votre dernière réservation ? Il vous suffit de renseigner votre adresse mail dans le champs suivant. Un mail de confirmation sera envoyé à cette même adresse.</p>

                <p><strong>Nous vous rappelons qu'une annulation peut être effectuer au plus tard la veille de l'intervention. Sans cela, nous devrons facturer l'intervention au tarif normal. Merci de votre compréhension.</strong></p>
            </div>
            <div class="col-6 mx-auto text-center">
                <form class="delete-reservation__form" method="post" action="<?php echo admin_url('admin-post.php') ?>">
                    <div class="row">
                        <div class="col-12">
                            <input type="hidden" name="action" value="deleteEmail">
                            <input name="email" type="email" placeholder="Entrer l'adresse mail utilisée pour la réservation" required />
                        </div>
                        <div class="col-12 my-3">
                            <input type="checkbox" id="scales" name="validateDelete" value="Test" class="d-inline" required>
                            <label for="validateDelete" class="d-inline">En cochant cette case, je valide mon annulation. J'ai conscience qu'un mail sera envoyé à l'entreprise Cycloplombier lui notifiant cette information, ainsi l'intervention que j'ai demandé n'aura donc pas lieu. </label>
                        </div>
                        <div class="col-12">
                            <input type="submit" id="accept-button" value="Valider l'annulation" class="w-100" />
                            <?php wp_nonce_field('delete_user_reservation'); ?>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
    </main>
</div>
<?php get_footer(); ?>
<?php include(TEMPLATEPATH . "/resa.php"); ?>