<?php
/*
Template Name: Blog archives
*/
?>
<?php get_header(); ?>
<div class="site-content blog">
    <section class="container-fluid entete-pages">
        <div class="overlay"></div>
        <img src="<?php the_field('img_blog'); ?>" class="img-fluid" alt="<?php the_field('alt_thumbnail') ?>">
        <h1 class="text-center"><?php the_field('titre_h1'); ?></h1>
    </section>
    <section class="container s-last_articles">
        <?php include(TEMPLATEPATH . "/breadcrumb.php"); ?>
        <div class="resume_article">
            <div class="row">
                <div class="col-12">
                    <p class="titre-archive">Dernier article</p>
                </div>

                <?php
                $sticky = get_option('sticky_posts');
                $args = array(
                    'posts_per_page'      => 1,
                    'post__in'            => $sticky,
                    'ignore_sticky_posts' => 1,
                );
                $query = new WP_Query($args);
                $postslist = get_posts($args);
                foreach ($postslist as $post) :  setup_postdata($post); ?>
                    <div class="col-12  one_resume_article">
                        <div class="row">
                            <div class="col-12 col-md-5">
                                <a href="<?php the_permalink() ?>"><?php the_post_thumbnail('post-thumbnail', ['class' => 'img-fluid', 'title' => 'Feature image']); ?></a>
                            </div>
                            <div class="col-12 col-md-7 resume">
                                <h2><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
                                <span class="date">
                                    <?php the_date(); ?> -
                                </span>
                                <span class="flag">
                                    <?php the_category(', '); ?>
                                </span>
                                <p><?php the_excerpt(); ?></p>
                                <a href="<?php the_permalink() ?>"><span class="read_more">Lire la suite</span></a>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </section>
    <section class="container s-articles_show">
        <div class="col-12">
            <p class="titre-archive">Toutes les actus</p>
        </div>
        <div class="col-12">
            <div class="row">
                <div class="col-12 col-md-8">
                    <?php
                    $catquery = new WP_Query('offset=1&cat=2&posts_per_page=10'); ?>
                    <?php while ($catquery->have_posts()) : $catquery->the_post(); ?>
                        <div class="row one_actu">
                            <div class="col-12 col-md-4 img_article">
                                <a href="<?php the_permalink() ?>">
                                    <?php the_post_thumbnail('post-thumbnail', ['class' => 'img-fluid', 'title' => 'Feature image']); ?></a>
                            </div>
                            <div class="col-12 col-md-8 resume_article">
                                <a href="<?php the_permalink() ?>">
                                    <h2><?php the_title(); ?></h2>
                                </a>
                                <span class="date">
                                    <?php the_time(get_option('date_format')); ?> -
                                </span>
                                <span class="flag">
                                    <?php the_category(', '); ?>
                                </span><br>
                                <?php the_excerpt(); ?>
                                <a href="<?php the_permalink() ?>"><span class="read_more">Lire la suite</span></a>
                            </div>
                        </div>
                    <?php endwhile; ?>
                    <?php wp_reset_postdata(); ?>
                </div>
                <div class="col-12 col-md-4">
                    <h3 class="h3_press">Presse</h3>
                    <div class="col-12">
                        <?php
                        $catquery = new WP_Query('offset=0&cat=3&posts_per_page=5'); ?>
                        <?php while ($catquery->have_posts()) : $catquery->the_post(); ?>
                            <div class="row one_press">
                                <a href="<?php the_permalink() ?>">
                                    <div class="img_press">
                                        <?php the_post_thumbnail('post-thumbnail', ['class' => 'img-fluid', 'title' => 'Feature image']); ?>
                                    </div>

                                    <div class="block_text_press w-100 tetx-left">
                                        <a href="<?php the_permalink() ?>">
                                            <h2><?php the_title(); ?></h2>
                                        </a>
                                        <span class="date">
                                            <?php the_time(get_option('date_format')); ?> -
                                        </span>
                                        <span class="flag">
                                            <?php the_category(', '); ?>
                                        </span><br><br>
                                    </div>
                                </a>
                            </div>
                        <?php endwhile; ?>
                        <?php wp_reset_postdata(); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php get_footer(); ?>
    <?php include(TEMPLATEPATH . "/resa.php"); ?>
</div>
</div>
</div>