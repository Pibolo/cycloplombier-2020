            <footer class="container-fluid footer">

              <section class="container f-partners">
                <p class="text-center">Partenaires et Supports</p>
                <?php
                if (have_rows('partenaires', 'option')) :
                ?>
                  <div class="owl-carousel owl-theme">
                    <?php
                    while (have_rows('partenaires', 'option')) : the_row();
                    ?>
                      <div class="item">
                        <a href="<?php the_sub_field('lien_partenaire', 'option'); ?>" alt="Lien partenaires du Cycloplombier"><img src="<?php the_sub_field('logo_partenaire'); ?>" class="img-fluid" alt="<?php the_sub_field('texte_alt', 'option'); ?>"></a>
                      </div>
                    <?php endwhile; ?>
                  </div>
                <?php
                else :
                endif;
                ?>
              </section>
              <section class="container-fluid f-infos">
                <div class="container d-flex flex-column">
                  <div class="row">
                    <div class="col-lg-4 col-sm-6 order-sm-2 order-lg-1">
                      <img src="<?php bloginfo('template_directory'); ?>/assets/img/logo_footer.png" class="img-fluid d-block mx-auto" alt="Logo Cycloplombier depuis 2014" title="Logo du Cycloplombier">
                    </div>
                    <div class="col-lg-4 col-sm-12 order-sm-1 order-lg-2">
                      <p><?php the_field('reservation-footer-title', 'option'); ?></p><br>
                      <a href="<?php the_field('reservation_link', 'option'); ?>" class="btn_reservation" alt="lien vers la réservation d'un plombier" title="Réservez une intervention">Réserver</a>
                    </div>
                    <div class="col-lg-4 col-sm-6 order-sm-3 order-lg-3 f-btn_social">
                      <div class="row">
                        <div class="col-4">
                          <a href="<?php the_field('instagram_link', 'option'); ?>" alt="lien instagram du Cycloplombier"><img src="<?php bloginfo('template_directory'); ?>/assets/img/insta.png" class="img-fluid" alt="Logo Instagram @Cycloplombier" title="Instagram du cycloplombier"></a>
                        </div>
                        <div class="col-4">
                          <a href="<?php the_field('twitter_link', 'option'); ?>" alt="lien twitter du Cycloplombier"><img src="<?php bloginfo('template_directory'); ?>/assets/img/twitter.png" class="img-fluid" alt="Logo Twitter @Cycloplombier" title="twitter du cycloplombier"></a>
                        </div>
                        <div class="col-4">
                          <a href="<?php the_field('facebook_link', 'option'); ?>" alt="lien facebook du Cycloplombier"><img src="<?php bloginfo('template_directory'); ?>/assets/img/facebook.png" class="img-fluid" alt="Logo Facebook @Cycloplombier" title="facebook du cycloplombier"></a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
              <section class="container-fluid f-social">
                <div class="container">
                  <div class="row">
                    <div class="col-12">
                      <div class="row">
                        <?php echo do_shortcode('[instagram-feed]'); ?>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
              <section class="container-fluid f-mentions-menu">
                <div class="container">
                  <div class="col-12">
                    <div class="row">
                      <div class="col-lg-4 col-sm-6 legal">
                        <img src="<?php bloginfo('template_directory'); ?>/assets/img/logo-artisan-cycloplombier.jpg" alt="Logo Artisan Cycloplombier" title="Logo Artisan Cycloplombier" class="float-left">
                        <address>SASU LE CYCLO PLOMBIER
                          Adress 27, QUAI DE L'OISE
                          75019 PARIS
                          Téléphone <a class="tel__link" href="tel:+33176381326">01 76 38 13 26</a></address>
                        <br><br>
                        <p>R.C.S PARIS 818938938 <br>
                          Numéro de TVA Intracommunautaire : FR 50818938938 <br>
                          Assurance de Responsabilité décenale N°
                          175690471 MAAF Assurance S.A - Chauray - 79063 Niort Cedex 9 <br>
                          Garantie accordée sur les chantiers exécutés en France et en principauté de Monaco</p>
                      </div>
                      <div class="col-lg-4 col-sm-6 payment">
                        <img src="<?php bloginfo('template_directory'); ?>/assets/img/logo-CB-cycloplombier.jpg" alt="logo Paiement par carte bancaire Cycloplombier" title="Paiement par carte bancaire Cycloplombier">
                      </div>
                      <div class="col-lg-4 col-sm-12 f-menu">
                        <?php
                        wp_nav_menu(
                          $menuParameters = array(
                            'menu' => 'main',
                            'theme_location' => 'footer-menu',
                            'container' => 'nav',
                            'echo' => false, // parent container
                            'container_class' => 'nav flex-column', //parent container ID
                            'depth' => 1,
                            'items_wrap' => '%3$s',
                            'walker' => new description_walker() // add ul
                          )
                        );
                        echo strip_tags(wp_nav_menu($menuParameters), '<nav><a></nav>' . '<div class="menu-mobil">
                          <div class="menu-btn_social">
                              <a href="#"><i class="fab fa-instagram"></i></a>
                              <a href="#"><i class="fab fa-google-plus-square"></i></a>
                              <a href="#"><i class="fab fa-twitter-square"></i></a>
                              <a href="#"><i class="fab fa-facebook-square"></i></a>
                          </div>
                          <div class="menu-intervention text-center">
                            <p>Intervention 7/7 Jours <br><strong>PRENDRE RDV</strong> <br>de 7h à 17h</p><br>
                          </div>
                        </div>');
                        ?>
                      </div>
                      <div class="col-12">
                        <p><?php the_field('mentions_tarif', 'option'); ?></p>

                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </footer>
            <div class="site-cache"></div>
            </div>

            </div>
            <?php wp_footer(); ?>
            <!-- POPIN HOME -->
            </body>

            </html>