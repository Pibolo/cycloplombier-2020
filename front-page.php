<?php
add_filter('excerpt_length', function ($length) {
  return 20;
}, 999);

get_header();
?>

<div class="site-content">
  <section class="container-fluid section1">
    <div class="overlay"></div>
    <img src="<?php the_field('img_home'); ?>" class="img-fluid" alt="Photo représentant le vélo cargo du Cycloplombier sous un pont" title="fidèle destrier du Cycloplombier : son vélo cargo">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 col-sm-12 order-md-1 order-2">
          <div class="s1_presentation_cyclo">
            <h1><?php the_field('Home_H1'); ?></h1><br>
            <h2><?php the_field('Home_H2'); ?></h2><br>
            <p><?php the_field('desc'); ?></p>
          </div>
        </div>
        <div class="col-lg-6 offset-lg-0 col-sm-8 offset-sm-2 order-md-2 order-1">
          <div class="s1-info-resa">
            <div class="s1_tarif">
              <div class="tarif_unique">
                <p>Forfait 1H</p>
                <span><?php the_field('nombre_Tarif_unique'); ?>€<sup style="font-size:30px;">*</sup></span>
              </div>
              <?php
              if (have_rows('repeater_field_intervention')) : ?>
                <ul class="nav flex-column">
                  <?php
                  while (have_rows('repeater_field_intervention')) : the_row(); ?>


                    <li class="nav-item"><span><?php the_sub_field('sub_field_intervention'); ?></span></li>

                  <?php endwhile; ?>
                </ul>
              <?php
              else :
              endif;

              ?>

            </div>

            <a class="btn btn_red d-block mx-auto" href="<?php the_field('reservation_link'); ?>" title="Réservez">Réserver</a>

          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="container-fluid section2">
    <div class="container">

      <h2 class="mb-5 mt-4 pb-4">
        <?php the_field('presentation_title_h2'); ?>
      </h2>
      <div class="row valeurs mb-2">
        <div class="col-lg-6 mb-5 pr-md-5">
          <div class="d-flex">
            <h3><?php the_field('presentation_title1'); ?></h3>
          </div>
          <img src="<?php the_field('image_service_1'); ?>" title="Vélo cargo du Cycloplombier" class="img-fluid mb-4" alt="image vélo cargo Cycloplombier"><br>
          <div class="box">
            <a href="#" class="showText text-right">
            </a>
            <div class="hidden-box d-block">
            <p><?php the_field('presentation_text1'); ?></p>
            </div>
          </div>
        </div>
        <div class="col-lg-6 mb-5 pl-md-5">
          <div class="d-flex">
            <h3><?php the_field('presentation_title2'); ?></h3>
          </div>
          <img src="<?php the_field('image_service_2'); ?>" title="Devis du plombier à vélo" class="img-fluid mb-4" alt="image devis Cycloplombier"><br>
          <div class="box">
            <a href="#" class="showText text-right">
            </a>
            <div class="hidden-box d-block">
            <p><?php the_field('presentation_text2'); ?></p>
            </div>
          </div>
        </div>
        <div class="col-lg-6 mb-5 pr-md-5">
          <div class="d-flex">
            <h3><?php the_field('presentation_title3'); ?></h3>
          </div>
          <img src="<?php the_field('image_service_3'); ?>" title="Les intervention du plombier" class="img-fluid mb-4" alt="image des interventions du Cycloplombier"><br>
          <div class="box">
            <a href="#" class="showText text-right">
            </a>
            <div class="hidden-box d-block">
            <p><?php the_field('presentation_text3'); ?></p>
            </div>
          </div>
        </div>
        <div class="col-lg-6 mb-5 pl-md-5">
          <div class="d-flex">
            <h3><?php the_field('presentation_title4'); ?></h3>
          </div>
          <img src="<?php the_field('image_service_4'); ?>" title="Facturation du plombier" class="img-fluid mb-4" alt="image de la facturation du Cycloplombier"><br>
          <div class="box">
            <a href="#" class="showText text-right">
            </a>
            <div class="hidden-box d-block">
            <p><?php the_field('presentation_text4'); ?></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <div class="container-fluid bg-accroche p-4 text-center">
    <?php the_field('accroche_home'); ?>
  </div>
  <section class="section2 pt-4">
    <div class="container pt-3">
      <div class="row">
        <div class="col-lg-4">
          <div class="card">
            <a href="<?php the_field('link_block'); ?>" title="Vos Questions">
              <img src="<?php the_field('img_block_1'); ?>" alt="Vos Questions" class="img-fluid">
              <div class="card-body">
                <h3 class="card-title"><?php the_field('titre_block_1') ?></h3>
                <p class="card-text"><?php the_field('text_block_1') ?></p>
              </div>
            </a>
          </div>
        </div>

        <div class="col-lg-4">
          <div class="card">
            <a href="<?php the_field('link_block2'); ?>" title="Nous rejoindre">
              <img src="<?php the_field('img_block_2'); ?>" alt="Nous rejoindre" class="img-fluid">
              <div class="card-body">
                <h3 class="card-title"><?php the_field('titre_block_2') ?></h3>
                <p class="card-text"><?php the_field('text_block_2') ?></p>
              </div>
            </a>
          </div>
        </div>

        <div class="col-lg-4">
          <div class="card">
            <a href="<?php the_field('link_block3'); ?>" title="Réservez une intervention">
              <img src="<?php the_field('img_block_3'); ?>" alt="Réservez une intervention" class="img-fluid">
              <div class="card-body">
                <h3 class="card-title"><?php the_field('titre_block_3') ?></h3>
                <p class="card-text"><?php the_field('text_block_3') ?></p>
              </div>
            </a>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="container-fluid section3">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 col-md-12">
          <div id="carouselExampleControls" class="carousel vert slide" data-ride="carousel" data-interval="3000">
            <h3>Ce que vous en dites</h3>
            <ol class="carousel-indicators">
              <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
              <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
              <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">

              <?php
              $counter = 0;
              if (have_rows('repeater_field_avis')) : ?>
                <?php
                while (have_rows('repeater_field_avis')) : the_row() || $counter ?>
                  <div class="carousel-item <?php if ($counter < 1) {
                                              echo 'active';
                                            } ?>">
                    <div class="block_avis">
                      <i class="fas fa-star"></i>
                      <i class="fas fa-star"></i>
                      <i class="fas fa-star"></i>
                      <i class="fas fa-star"></i>
                      <i class="fas fa-star"></i><br>
                      <br>
                      <p><?php the_sub_field('Avis_client') ?><br>
                        <i class="float-right"><?php the_sub_field('Nom_Client') ?></i><br>
                      </p>
                    </div>
                  </div>
                <?php $counter++;
                endwhile; ?>
              <?php
              else :
              endif;
              ?>

            </div>
            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon bg-dark rounded-circle" aria-hidden="true"></span>
              <span class="sr-only">Précédent</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
              <span class="carousel-control-next-icon bg-dark rounded-circle" aria-hidden="true"></span>
              <span class="sr-only">Suivant</span>
            </a>
          </div>
        </div>
        <div class="col-12 col-lg-6">
          <div class="s3-actus">
            <?php
            $catquery = new WP_Query('&posts_per_page=1'); ?>
            <?php while ($catquery->have_posts()) : $catquery->the_post(); ?>
              <div class="s3-one-actu">
                <div class="row">
                  <div class="col-12">
                    <a href="<?php the_permalink() ?>">
                      <?php the_post_thumbnail('post-thumbnail', ['class' => 'img-fluid', 'title' => 'Feature image']); ?>
                    </a>
                  </div><br><br>
                  <div class="col-12">
                    <a href="<?php the_permalink(); ?>">
                      <div class="resume-title-article"><?php the_title(); ?></div><br>
                      <span class="s3-actu-resume">
                        <?php the_excerpt(); ?>
                      </span>
                    </a>
                  </div>
                  <div class="col-12">
                    <div class="row">
                      <span class="s3-actu-flag col-6 col-md-8">
                        <?php the_category('single'); ?>
                        <span class="s3-actu-date">
                          <?php the_time(get_option('date_format')); ?>
                        </span>
                      </span>
                      <div class="linkRed col-6 col-md-4">
                        <a href="<?php the_permalink(); ?>" class="text-right">Lire la suite</a>
                      </div>
                    </div>
                  </div>

                  <div class="col-12 pt-3">
                    <div class="link_more_actu text-center">
                      <a href="<?php echo get_permalink(get_option('page_for_posts')); ?>" class="btn_red btn_actu">Toutes nos actus</a>
                    </div>
                  </div>

                </div>
                <div style="clear:both;"></div>
              </div><br>
            <?php endwhile; ?>
            <?php wp_reset_postdata(); ?>
          </div>
        </div>
      </div>
    </div>
  </section>

  <?php get_footer(); ?>
  <?php include(TEMPLATEPATH . "/resa.php"); ?>