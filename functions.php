<?php

add_action('wp_enqueue_scripts', 'cycloplombier_scripts');
function cycloplombier_scripts()
{
  wp_register_script('jquery', get_template_directory_uri() . '/assets/js/jquery/jquery-3.3.1.min.js', array(), '3.3.1', true);
  wp_register_script('bootstrap', get_template_directory_uri() . '/assets/js/bootstrap/bootstrap.min.js', array('jquery'), '4.0.0', true);
  wp_register_script('owl', get_template_directory_uri() . '/assets/js/owl.carousel.min.js', array('jquery'), '4.0.0', true);
  wp_register_script('jquery-confirm', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js', array('jquery'), '', true);
  wp_register_script('main', get_template_directory_uri() . '/assets/js/main.min.js', array('jquery', 'bootstrap', 'owl', 'jquery-confirm'), '1.0', true);
  wp_enqueue_script('main');
}


function prefix_add_footer_styles()
{
  wp_register_style('style-general', get_template_directory_uri() . '/assets/css/styles.css', array(), '1.0', 'all', true);
  wp_register_style('style-bootstrap', get_template_directory_uri() . '/assets/css/bootstrap/bootstrap.min.css', array(), '4.0.0', 'all');
  wp_register_style('style-font', 'https://fonts.googleapis.com/css?family=Titillium+Web:200,300,400,700&display=swap', 'all');
  wp_register_style('style-owl', get_template_directory_uri() . '/assets/css/owl.carousel.min.css', array(), '2.3.4', 'all');
  wp_register_style('style-font-awesome', 'https://use.fontawesome.com/releases/v5.8.1/css/all.css', array(), '5.8.1', 'all');
  wp_register_style('style-jqueryConfirm', 'https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css', array(), '', 'all');
  wp_register_style('style-owl-theme', get_template_directory_uri() . '/assets/css/owl.theme.default.min.css', array('style-general', 'style-bootstrap', 'style-font', 'style-owl', 'style-jqueryConfirm', 'style-font-awesome'), '2.3.4', 'all');
  wp_enqueue_style('style-owl-theme');
};
add_action('get_footer', 'prefix_add_footer_styles');

function itsme_disable_feed()
{
  wp_die(__('No feed available, please visit the <a href="' . esc_url(home_url('/')) . '">homepage</a>!'));
}

add_action('do_feed', 'itsme_disable_feed', 1);
add_action('do_feed_rdf', 'itsme_disable_feed', 1);
add_action('do_feed_rss', 'itsme_disable_feed', 1);
add_action('do_feed_rss2', 'itsme_disable_feed', 1);
add_action('do_feed_atom', 'itsme_disable_feed', 1);
add_action('do_feed_rss2_comments', 'itsme_disable_feed', 1);
add_action('do_feed_atom_comments', 'itsme_disable_feed', 1);

add_action('send_headers', 'add_header_xua');
function add_header_xua()
{
  header('Strict-Transport-Security: max-age=0;');
}

function wpm_login_style()
{ ?>
  <style type="text/css">
    #login h1 a,
    .login h1 a {
      background-image: url("<?php echo get_template_directory_uri(); ?>/assets/img/logo_black.png");
    }
  </style>
<?php }
add_action('login_enqueue_scripts', 'wpm_login_style');


// AJOUT IMAGE A LA UNE
add_theme_support('post-thumbnails');

add_action('wp_footer', 'print_my_script', 100);
function print_my_script()
{
?>
  <script>
    jQuery(document).ready(function($) {
      $('.owl-carousel').owlCarousel({
        loop: true,
        margin: 10,
        dots: false,
        autoplay: true,
        responsiveClass: true,
        navElement: true,
        responsive: {
          0: {
            items: 1,
            nav: true
          },
          600: {
            items: 3,
            nav: true,
          },
          1000: {
            items: 5,
            nav: true,
            loop: true,
            margin: 20
          }
        }
      });
    });
  </script>
<?php
}

// LIMITE EXERPT
function wpdocs_custom_excerpt_length($length)
{
  return 50;
}
add_filter('excerpt_length', 'wpdocs_custom_excerpt_length', 999);


// MENUS
function register_my_menus()
{
  register_nav_menus(
    array(
      'header-menu' => __('Header Menu'),
      'header-menu-sup' => __('Header Menu sup'),
      'footer-menu' => __('Footer Menu')
    )
  );
}
add_action('init', 'register_my_menus');



class description_walker extends Walker_Nav_Menu
{
  public function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0)
  {
    global $wp_query;
    $indent = ($depth) ? str_repeat("\t", $depth) : '';

    $class_names = $value = '';

    $classes = empty($item->classes) ? array() : (array) $item->classes;

    $class_names = join(' ', apply_filters('nav_menu_css_class', array_filter($classes), $item));
    // $class_names = ' class="'. esc_attr( $class_names ) . '"';

    $output .= $indent . '<ul id="' . $item->ID . '"' . $value . $class_names . '>';

    $attributes  = !empty($item->attr_title) ? ' title="'  . esc_attr($item->attr_title) . '"' : '';
    $attributes .= !empty($item->target)     ? ' target="' . esc_attr($item->target) . '"' : '';
    $attributes .= !empty($item->xfn)        ? ' rel="'    . esc_attr($item->xfn) . '"' : '';
    $attributes .= !empty($item->url)        ? ' href="'   . esc_attr($item->url) . '"' : '';

    $prepend = '<strong>';
    $append = '</strong>';
    $description  = !empty($item->description) ? '<span>' . esc_attr($item->description) . '</span>' : '';

    if ($depth != 0) {
      $description = $append = $prepend = "";
    }

    $item_output = $args->before;
    $item_output .= '<a' . $attributes . ' class="' . $class_names . "\">";
    $item_output .= $args->link_before . $prepend . apply_filters('the_title', $item->title, $item->ID) . $append;
    $item_output .= $description . $args->link_after;
    $item_output .= '</a>';
    $item_output .= $args->after;

    $output .= apply_filters('walker_nav_menu_start_el', $item_output, $item, $depth, $args);

    if ($item->menu_order == 1) {
      $classes[] = 'first';
    }
  }
}

// ADD CUSTOM LINKS MENU
function new_nav_menu_items($items)
{
  return $items;
}

add_filter('wp_nav_menu_items', 'new_nav_menu_items');

function my_home_category($query)
{
  if ($query->is_home() && $query->is_main_query()) {
    $query->set('cat', '4');
  }
}
add_action('pre_get_posts', 'my_home_category');

add_filter('acf/format_value/type=textarea', 'do_shortcode');

add_filter('img_caption_shortcode_width', '__return_false');

function imageOnly($deprecated, $attr, $content = null)
{
  return do_shortcode($content);
}
add_filter('img_caption_shortcode', 'imageOnly', 10, 3);



//CHANGE LA CLASSE DE L'IMAGE DE the_content par IMG-FLUID
function add_class_to_img_and_p_in_content($content)
{
  $pattern = "/<img(.*?)class=\"(.*?)\"(.*?)>/i";
  $replacement = '<img$1class="$2 img-fluid"$3>';

  $content = preg_replace($pattern, $replacement, $content);

  $pattern = "/<p(.*?)class=\"(.*?)\"(.*?)>/i";
  $replacement = '<p$1class="$2 newclass newclasstwo"$3>';

  $content = preg_replace($pattern, $replacement, $content);
  return $content;
}
add_filter('the_content', 'add_class_to_img_and_p_in_content', 11);

if (function_exists('acf_add_options_page')) {
  acf_add_options_sub_page(array(
    'page_title'  => 'Réglages Footer',
    'menu_title'  => 'Footer',
    'parent_slug' => 'edit.php?post_type=page',
  ));

  acf_add_options_page(array(
    'page_title'    => 'Congés',
    'menu_title'    => 'Congés',
    'menu_slug'     => 'options-generales',
    'position' =>   5,
  ));
  acf_add_options_page(array(
    'page_title'    => 'Token QBO',
    'menu_title'    => 'Token QBO',
    'menu_slug'     => 'my-theme-options',
    'position' =>   122,
  ));
}

remove_action("wp_head", "wp_generator");

add_filter('login_errors', '__return_null');

function yt_cache_enable($return)
{
  if (class_exists('autoptimizeCache')) {
    return true;
  }

  return $return;
}

define('FS_METHOD', 'direct');

function w_get_insta($args = array())
{
  require 'lib/vendor/autoload.php';
  $args = wp_parse_args($args, array(
    'user'  => 'cyclo_plombier',
  ));
  $feed = fetch_insta($args);
  if (!empty($feed)) {
    return $feed->medias;
  }
  return false;
}

function fetch_insta($args = array())
{
  $args = wp_parse_args($args, array(
    'user'  => 'cyclo_plombier',
  ));
  $transient_name = "cyclo_plombier_from_{$args['user']}";
  if (false === ($insta_feed = get_transient($transient_name))) {
    $path = WP_CONTENT_DIR . '/instagram';
    if (!is_dir($path)) {
      mkdir($path, 0755, true);
    }
    try {
      $cache = new Instagram\Storage\CacheManager($path);
      $api   = new Instagram\Api($cache);
      $api->setUserName($args['user']);

      $insta_feed = $api->getFeed();
    } catch (Exception $e) {
      return false;
    }
    set_transient($transient_name, $insta_feed, HOUR_IN_SECONDS * 4);
  }
  return $insta_feed;
}

function w_format_insta($insta)
{
  $date = date_i18n('d F', strtotime($insta->date->date));
  $img  = end($insta->thumbnails)->src;
  $link = $insta->link;
  return vsprintf('<div class="col-6 col-sm-3 col-md-3 p-2">
      <a href="%2$s" class="" alt="Lien des images du Cycloplombier provenant d\Instagram" target="_blank">
        <img src="%1$s" alt="Image Cycloplombier provenant d\'Instagram" title="Cycloplombier sur Instagram" class="img-fluid" />
      </a>
  </div>', array(
    $img,
    $link,
  ));
}

function wpf_dev_limit_date_picker()
{
  $repeater   = 'pick_leave_dates';
  $date_start = 'date_de_debut';
  $date_end   = 'date_de_fin';
  $opts = get_option("options_{$repeater}");
  $list = [];
  for ($i = 0; $i < $opts; $i++) {
    $list[] = [
      get_option("options_{$repeater}_{$i}_{$date_start}"),
      get_option("options_{$repeater}_{$i}_{$date_end}"),
    ];
  } ?>
  <script type="text/javascript">
    var vacances = [
      <?php
      if (!empty($list)) {
      foreach ($list as $value) {
        echo '[new Date(' . substr($value[0], 0, 4) . ', ' . (intval(substr($value[0], 4, 2)) - 1) . ', ' . substr($value[0], 6, 2) . ', 0,0,0,0), new Date(' . substr($value[1], 0, 4) . ',' . (intval(substr($value[0], 4, 2)) - 1) . ',' . substr($value[1], 6, 2) . ', 23,59,59,0)],';
      } 
    }?>
      
    ]
    window.wpforms_datepicker = {
      locale: {
        firstDayOfWeek: 1,
        weekdays: {
          shorthand: ['Dim', 'Lun', 'Mar', 'Mer', 'Jeu', 'Ven', 'Sam'],
          longhand: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi']
        },
        months: {
          shorthand: ['Janv', 'Févr', 'Mars', 'Avr', 'Mai', 'Juin', 'Juil', 'Août', 'Sept', 'Oct', 'Nov', 'Déc'],
          longhand: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre']
        },
      },
      disableMobile: true,
      // Don't allow users to pick past dates
      minDate: new Date(),
      // Don't allow users to pick Sundays
      enable: [
        function(dateObj) {
          for (var i = 0; i < vacances.length; i++) {
            if (dateObj >= vacances[i][0] && dateObj <= vacances[i][1]) {
              return false;
            }
          }
          return -1 === [0, 6, 0].indexOf(dateObj.getDay());
        }
      ]
    }
  </script>
<?php
}

add_filter('avalaible_timeslot_for_a_day', 'disable_cyclo_timeslot', 10, 2);
add_action('wpforms_wp_footer', 'wpf_dev_limit_date_picker');
function disable_cyclo_timeslot($timeslot, $day)
{
  $repeater   = 'creneaux_par_date';
  $date_creneaux = 'date_creneaux';
  $heure_creneaux   = 'heure_creneaux';
  $opts = get_option("options_{$repeater}");

  for ($i = 0; $i < $opts; $i++) {
    $date = DateTime::createFromFormat('Ymd', get_option("options_{$repeater}_{$i}_{$date_creneaux}"));
    $testDate = $date->format('Y-m-d');
    if ($testDate == $day) {
      $timeslot[] =  get_option("options_{$repeater}_{$i}_{$heure_creneaux}");
    }
  }
  return $timeslot;
}




add_filter('avalaible_timeslot_for_a_day', 'disable_x_hours_before', 10, 2);

function disable_x_hours_before($timeslot, $day)
{
  if (date('Y-m-d') === $day) {
    $hours = array(
      '9:30' => strtotime('today 09:30'),
      '10:00' => strtotime('today 10:00'),
      '11:00' => strtotime('today 11:00'),
      '12:00' => strtotime('today 12:00'),
      '14:00' => strtotime('today 14:00'),
      '15:00' => strtotime('today 15:00'),
      '16:00' => strtotime('today 16:00'),
      '17:00' => strtotime('today 17:00'),
    );
    $pastime = time() + 3600 * 12;
    foreach ($hours as $k => $value) {
      error_log($value . " " . $pastime);
      if (intval($value) < intval($pastime)) {
        $timeslot[] =  $k;
        $timeslot[] =  $k;
      }
    }
    error_log(var_export($timeslot, true));
  }
  return $timeslot;
}



add_action('wp_head', 'init_js_vars', 0);
function init_js_vars()
{
  echo '<script>var wpforms_pageScroll = false;</script>';
}

function stop_plugin_update($value)
{
  unset($value->response['wpforms/wpforms.php']);
  return $value;
}
add_filter('site_transient_update_plugins', 'stop_plugin_update');




function send_delete_reservation($emailCorres)
{
    $benz_admin_email = get_bloginfo('admin_email');
    wp_mail($benz_admin_email, 'Annulation de réservation ', 'Une réservation a été annulée, mail correspondant à l\'annulation : ' . $emailCorres);
}
function send_delete_reservation_customer($emailCorres)
{
    wp_mail($emailCorres, 'Votre réservation a bien été annulée', 'Votre réservation a bien été annulée. Merci et à bientôt. L\'équipe Cycloplombier.');
}


function delete_reservation_from_customer($customer_email)
{
  global $wpdb;
  $e_id = $wpdb->get_var($wpdb->prepare(
    "SELECT * FROM {$wpdb->prefix}wpforms_entries
      WHERE JSON_EXTRACT(fields, '$.45.value')
      LIKE '%$customer_email%'
      AND form_id = 240
      ORDER BY entry_id
      DESC LIMIT 1",
      
  ));
  if ($e_id) {
    $wpdb->query("DELETE FROM {$wpdb->prefix}wpforms_entries WHERE entry_id = {$e_id}");
    send_delete_reservation($customer_email);
    send_delete_reservation_customer($customer_email);
    return true;
  }
  return false;
}



function delete_user_reservation()
{
  $email = $_REQUEST['email'];
  if (!empty($email)) {
    $result = delete_reservation_from_customer($email);
    wp_redirect(site_url('/annulation-dune-reservation-confirmee/'));
    if (empty($result)) {
      wp_redirect(site_url('/annulation-adresse-inconnue/'));
    }
  }
}
add_action('admin_post_nopriv_deleteEmail', 'delete_user_reservation');
add_action('admin_post_deleteEmail', 'delete_user_reservation');


add_filter( 'https_local_ssl_verify', '__return_true' );
function custom_curl_timeout( $handle ){
	curl_setopt( $handle, CURLOPT_CONNECTTIMEOUT, 300 ); // 30 seconds. Too much for production, only for testing.
	curl_setopt( $handle, CURLOPT_TIMEOUT, 300 ); // 30 seconds. Too much for production, only for testing.
}

function custom_http_request_timeout( $timeout_value ) {
	return 300;
}

function custom_http_request_args( $r ){
	$r['timeout'] = 300;
	return $r;
}

function do_curl_filters() {
	add_action( 'http_api_curl', __NAMESPACE__ . '\custom_curl_timeout', 9999, 1 );
	add_filter( 'http_request_timeout', __NAMESPACE__ . '\custom_http_request_timeout', 9999 );
	add_filter( 'http_request_args', __NAMESPACE__ . '\custom_http_request_args', 9999, 1 );
}

function undo_curl_filters() {
	remove_action( 'http_api_curl', __NAMESPACE__ . '\custom_curl_timeout', 9999, 1 );
	remove_filter( 'http_request_timeout', __NAMESPACE__ . '\custom_http_request_timeout', 9999 );
	remove_filter( 'http_request_args', __NAMESPACE__ . '\custom_http_request_args', 9999, 1 );
}

function wpse_218377_nav_menu_css_class( $classes, $args ) {
  if ( ! empty( $args->theme_location ) && $args->theme_location === 'header-menu-sup' ) {
      $classes[] = 'footer--menu-item';
  }

  // ALWAYS return, not from inside the if
  return $classes;
}

add_filter( 'nav_menu_css_class' , 'wpse_218377_nav_menu_css_class' , 10, 3 );

wp_remote_get($url, array('sslverify' => FALSE));
add_filter('https_ssl_verify', '__return_false');
