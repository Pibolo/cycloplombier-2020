<?php
/*
Template Name: Page Défaut
*/
get_header(); ?>

<div class="site-content mentions">
  <section class="container-fluid entete-pages">
    <div class="overlay"></div>
    <?php the_post_thumbnail('post-thumbnail', ['class' => 'img-fluid', 'title' => 'Feature image']); ?>
    <h1 class="text-center"><?php the_field('titre_h1'); ?></h1>
  </section>
        <section class="container mb-5">

            <?php include(TEMPLATEPATH . "/breadcrumb.php"); ?>
            <?php the_content(); ?>
        </section>
    </main><!-- .site-main -->
</div><!-- .content-area -->

<?php get_footer(); ?>
<?php include(TEMPLATEPATH . "/resa.php"); ?>