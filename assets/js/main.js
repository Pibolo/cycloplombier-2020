(function($){

    $('#header__icon').click(function(e){
        e.preventDefault();
        $('body').toggleClass('with--sidebar');
    });

    $('#site-cache').click(function(e){
        $('body').removeClass('with--sidebar');
    });



    $('.header__icon').click(function(){
      $(this).toggleClass('open');
    });

    $('.bg_collapse').click(function(){
      $(this).toggleClass('Arrow_down Arrow_up');
    });

    $(document).ready(function() {
      $('#wpforms-240-field_11, #wpforms-240-field_12, #wpforms-240-field_63').addClass('row');
      $('.wpforms-image-choices-item').addClass('col-6 col-sm-4 col-md-4 col-lg-2');
      $('.choice-7').hide();
    });

    $( "#wpforms-240-field_47-city" ).removeAttr("required", true);
    $( "#wpforms-240-field_47-city" ).hide();
    $('input#wpforms-240-field_47-postal').replaceWith('<select name="wpforms[fields][47][postal]" id="wpforms-240-field_47-postal" class="wpforms-field-address-postal wpforms-field-required wpforms-valid" required><option value=" ">Choisissez votre localité</option><option value="75001 Paris">Paris 75001</option><option value="75002 Paris">Paris 75002</option><option value="75003 Paris">Paris 75003</option><option value="75004 Paris">Paris 75004</option><option value="75005 Paris">Paris 75005</option><option value="75006 Paris">Paris 75006</option><option value="75007 Paris">Paris 75007</option><option value="75008 Paris">Paris 75008</option><option value="75009 Paris">Paris 75009</option><option value="75010 Paris">Paris 75010</option><option value="75011 Paris">Paris 75011</option><option value="75012 Paris">Paris 75012</option><option value="75013 Paris">Paris 75013</option><option value="75014 Paris">Paris 75014</option><option value="75015 Paris">Paris 75015</option><option value="75016 Paris">Paris 75016</option><option value="75017 Paris">Paris 75017</option><option value="75018 Paris">Paris 75018</option><option value="75019 Paris">Paris 75019</option><option value="75020 Paris">Paris 75020</option><option value="94110 Arcueil">Arcueil 94110**</option><option value="93300 Aubervilliers">Aubervilliers 93300**</option><option value="93170 Bagnolet">Bagnolet 93006**</option><option value="92100 Boulogne-Billancourt">Boulogne-Billancourt 92100**</option><option value="94220 Charenton-Le-Pont">Charenton-Le-Pont 94220**</option><option value="92110 Clichy">Clichy 92110**</option><option value="94250 Gentilly">Gentilly 94250**</option><option value="92130 Issy les Moulineaux">Issy les Moulineaux 92130**</option><option value="94200 Ivry sur Seine">Ivry sur Seine 94200**</option><option value="94270 Kremlin-Bicêtre">Kremlin-Bicêtre 94270**</option><option value="93310 Le Pré Saint-Gervais">Le Pré Saint-Gervais 93310**</option><option value="93260 Les Lilas">Les Lilas 93260**</option><option value="92300 Levallois-Perret">Levallois-Perret 92300**</option><option value="92240 Malakoff">Malakoff 92240**</option><option value="93100 Montreuil">Montreuil 93100**</option><option value="92120 Montrouge">Montrouge 92120**</option><option value="92200 Neuilly Sur Seine">Neuilly Sur Seine 92200**</option><option value="93500 Pantin">Pantin 93500**</option><option value="93230 Romainville">Romainville 93230**</option><option value="94160 Saint Mandé">Saint Mandé 94160**</option><option value="93400 Saint-Ouen">Saint-Ouen 93400**</option><option value="92170 Vanves">Vanves 92170**</option><option value="94300 Vincennes">Vincennes 94300**</option>');
    if ($("body").hasClass("home")) {
      $(window).on("load", function () {
        $("#myModal").modal("show");
      });
    }
$('.sup-menu li:nth-child(1) > a').addClass('tel__link');
$('#navbarCollapse > ul.navbar-nav.ml-auto.d-md-flex.justify-content-between.w-md-100 > li:nth-child(8) > a').addClass('btn2 w-100 px-4');
$('.sup-menu li:nth-child(2) > a').addClass('btn btn_red w-100 align-items-center d-flex h-100 px-4');

})(jQuery);