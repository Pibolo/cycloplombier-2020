<?php
/*
Template Name: Confirmation de l'annulation
*/
get_header(); ?>
<div class="site-content delete-reservation">
    <section class="container-fluid entete-pages">
        <div class="overlay"></div>
        <?php the_post_thumbnail('post-thumbnail', ['class' => 'img-fluid', 'title' => 'Feature image']); ?>
        <h1 class="text-center"><?php the_field('titre_h1'); ?></h1>
    </section>
    <section class="container mb-5">
        <?php include(TEMPLATEPATH . "/breadcrumb.php");
        the_content();

        ?>
        <div class="row">
            <div class="col-12 text-center mt-4">
                <a class="btn_red d-block mx-auto" href="<?php echo home_url(); ?>"> Retour à l'accueil</a><br>
            </div>
            <div class="col-12 text-center">
                ou faire une <br><br><a class="btn_red d-block mx-auto" href="<?php echo site_url('/reservation'); ?>">Autre réservation</a>
            </div>

        </div>
    </section>
    </main>
</div>
<?php get_footer(); ?>
<?php include(TEMPLATEPATH . "/resa.php"); ?>