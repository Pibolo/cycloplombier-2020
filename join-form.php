<?php
/*
Template Name: Rejoignez-nous
*/

get_header(); ?>
<div class="site-content join-us">
  <section class="container-fluid entete-pages text-center">
    <div class="overlay"></div>
    <?php the_post_thumbnail('post-thumbnail', ['class' => 'img-fluid', 'title' => 'Feature image']); ?>
    <h1 class="text-center"><?php the_field('titre_h1'); ?></h1>
  </section>
  <div class="container mb-4">
    <?php include(TEMPLATEPATH . "/breadcrumb.php"); ?>
    <h2><?php the_field('titre_h2'); ?></h2>
    <?php the_field('chapeau'); ?><br><br>
    <div class="row">
      <div class="col-12 offset-0 col-md-8 offset-md-2">
        <?php echo do_shortcode('[wpforms id="118" title="false" description="false"]'); ?>
      </div>
    </div>
  </div>
  <?php get_footer(); ?>
  <?php include(TEMPLATEPATH . "/resa.php"); ?>
</div>
</div>
</div>
</div>