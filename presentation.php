<?php
/*
Template Name: Présentation
*/

get_header(); ?>

<div class="site-content presentation">
  <section class="container-fluid entete-pages">
    <h1 class="text-center"><?php the_field('titre_h1'); ?></h1>
  </section>
  <section class="s-presentation">
    <div class="container">
      <?php include(TEMPLATEPATH . "/breadcrumb.php"); ?>
      <div class="row">
        <div class="col-12">
          <h2><?php the_field('titre_philo'); ?></h2>
        </div>
        <div class="col-12 col-md-6">
          <div class="blocks_presa">
            <div class="img_presa">
              <img src="<?php bloginfo('template_directory'); ?>/assets/img/bg_smile.png" class="img-fluid" title="Le sourire du plombier" alt="Votre Cycloplombier a le smile">
            </div>
            <h3><?php the_field('titre_smile'); ?></h3>
            <?php the_field('texte_smile'); ?>
          </div>
        </div>
        <div class="col-12 col-md-6">
          <div class="blocks_presa">
            <div class="img_presa">
              <img src="<?php bloginfo('template_directory'); ?>/assets/img/bg_cyclo_vert.png" class="img-fluid" alt="Votre cycloplombier est vert" title="Votre cycloplombier est vert">
            </div>
            <h3><?php the_field('titre_vert') ?></h3>
            <?php the_field('texte_vert'); ?>
          </div>
        </div>
        <div class="col-md-12">
          <div class="block_contact_presa float-right">
            <h4><?php the_field('titre_contact_philo'); ?></h4>
            <a href="<?php the_field('lien_contact'); ?>" class="btn_red"><?php the_field('contact_link_text'); ?></a>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="s-presentation">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <h2><?php the_field('titre_presta'); ?></h2>
        </div>
        <div class="col-12 col-md-6">
          <div class="blocks_presa">
            <div class="img_presa">
              <img src="<?php bloginfo('template_directory'); ?>/assets/img/bg_reparation.png" class="img-fluid" title="Le sourire du plombier" alt="Votre Cycloplombier a le smile">
            </div>
            <h3><?php the_field('titre_reparation'); ?></h3>
            <?php the_field('texte_reparation'); ?>
          </div>
        </div>

        <div class="col-12 col-md-6">
          <div class="blocks_presa">
            <div class="img_presa">
              <img src="<?php bloginfo('template_directory'); ?>/assets/img/bg_devis.png" class="img-fluid" alt="Votre cycloplombier est vert" title="Votre cycloplombier est vert">
            </div>
            <h3><?php the_field('titre_devis'); ?></h3>
            <?php the_field('texte_devis'); ?>
          </div>
        </div>

        <div class="col-12 col-md-6">
          <div class="blocks_presa">
            <div class="img_presa">
              <img src="<?php bloginfo('template_directory'); ?>/assets/img/bg_reglement.png" class="img-fluid" alt="Votre cycloplombier est vert" title="Votre cycloplombier est vert">
            </div>
            <h3><?php the_field('titre_reglement'); ?></h3>
            <?php the_field('texte_reglement'); ?>
          </div>
        </div>
        <div class="col-12">
          <div class="block_contact_presa float-right">
            <h4><?php the_field('titre_link_resa'); ?></h4>
            <a href="<?php the_field('link_resa'); ?>" class="btn_red"><?php the_field('text_resa'); ?></a>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="s-presentation">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <h2><?php the_field('titre_pro'); ?></h2>
        </div>
        <div class="col-12">
          <div class="blocks_presa">
            <br>
            <div class="img_presa">
              <img src="<?php bloginfo('template_directory'); ?>/assets/img/bg_pros.png" class="img-fluid" title="Le sourire du plombier" alt="Votre Cycloplombier a le smile">
            </div>
            <h3><?php the_field('titre_depannage'); ?></h3>
            <?php the_field('texte_depannage'); ?>
          </div>
        </div>
        <div class="col-12">
          <div class="block_contact_presa float-right">
            <h4><?php the_field('titre_link_pro'); ?></h4>
            <a href="<?php the_field('lien_contact'); ?>" class="btn_red"><?php the_field('contact_link_text'); ?></a>
          </div>
        </div>
      </div>
    </div>
  </section>

  <?php get_footer(); ?>
</div>
</div>
<div class="site-cache" id="site-cache">
</div>
</div>
</div>