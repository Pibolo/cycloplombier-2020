<?php
/*
Template Name: FAQ
*/

get_header(); ?>

<div class="site-content faq">
  <section class="container-fluid entete-pages">
    <div class="overlay"></div>
    <img src="<?php the_field('img_faq'); ?>" class="img-fluid" alt="Photo représentant le vélo cargo du Cycloplombier sous un pont" title="fidèle destrier du Cycloplombier : son vélo cargo">
    <h1 class="text-center"><?php the_field('titre_h1'); ?></h1>
  </section>
  <section class="s-faq">
    <div class="container">
      <?php include(TEMPLATEPATH . "/breadcrumb.php"); ?>
      <div class="row">

        <div class="col-12">
        <?php the_content(['class' => 'img-fluid', 'title' => 'Feature image']); ?>
        </div>
        <?php
        if (have_rows('repeater_field_faq')) : ?>
          <?php
          while (have_rows('repeater_field_faq')) : the_row(); ?>
            <div class="col-12 block_faq">
                <h3 class="bg_collapse"><?php the_sub_field('sub_field_question'); ?></h3>
                <?php the_sub_field('sub_field_reponse'); ?>
            
            </div>
          <?php endwhile; ?>
        <?php
        else :
        endif;

        ?>
        <script>
          jQuery(document).ready(function($) {
            $('[data-toggle="collapse"]').click(function() {
              $(this).next('.collapse').collapse('toggle');
            });

          });
        </script>

      </div>
    </div>
  </section>

  <?php get_footer(); ?>
  <?php include(TEMPLATEPATH . "/resa.php"); ?>
</div>
</div>

</div>
</div>